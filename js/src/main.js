const md = new MobileDetect(window.navigator.userAgent);
if (md.mobile()) document.body.classList.add('page--mobile');

const supportSocial = document.querySelector('.support__social');

supportSocial && supportSocial.addEventListener('click', () => {
    supportSocial.classList.toggle('support__social--show');
});

const phoneInputs = document.querySelectorAll('input[type="tel"]');
phoneInputs.forEach((phone) => {
    IMask(phone, {mask: '+{38\\0} (00) 000-00-00'});
});

const responses = document.querySelectorAll('.response');

for (const response of responses) {
    const close = response.querySelector('.response__close');
    close.addEventListener('click', () => {
        response.classList.remove('response--visible');
    });
}

for (const form of document.querySelectorAll('.form')) {
    form.addEventListener('submit', (e) => {
        e.preventDefault();
        const form = e.target;
        const formData = new FormData();
        for (const element of form.elements) {
            if (element.type !== 'button') formData.append(element.name, element.value);
        }
        if (!formData.get('phone')) {
            const phoneInput = form.querySelector('[name="phone"]');
            phoneInput.classList.add('form__input--empty');
            phoneInput.addEventListener('animationend', () => {
                phoneInput.classList.remove('form__input--empty');
            });
            return false;
        }
        const xhr = new XMLHttpRequest();
        xhr.open('POST', form.action);
        xhr.onload = (e) => {
            const response = e.target;
            if (response.readyState === 4 && response.status === 200) {
                form.querySelector('.response').classList.add('response--visible');
                form.reset();
            }
        };
        xhr.send(formData);
    });
}

const overhead = document.querySelector('.overhead--header');

document.querySelector('.nav__switch').addEventListener('click', () => {
    document.querySelector('.nav').classList.toggle('nav--show');
    overhead.classList.toggle('overhead--show');
});

for (const option of document.querySelectorAll('.places__option')) {
    option.addEventListener('click', () => {
        const activeOption = 'places__option--active',
            activeList = 'places__list--visible',
            number = option.getAttribute('data-option');
        document.querySelector(`.${activeOption}`).classList.remove(activeOption);
        option.classList.add(activeOption);
        document.querySelector(`.${activeList}`).classList.remove(activeList);
        document.querySelector(`.places__list[data-list='${number}']`).classList.add(activeList);
    });
}

for (const option of document.querySelectorAll('.doctors__option')) {
    option.addEventListener('click', () => {
        const activeOption = 'doctors__option--active',
            activeList = 'doctors__content--visible',
            number = option.getAttribute('data-option');
        document.querySelector(`.${activeOption}`).classList.remove(activeOption);
        option.classList.add(activeOption);
        document.querySelector(`.${activeList}`).classList.remove(activeList);
        document.querySelector(`.doctors__content[data-list='${number}']`).classList.add(activeList);
    });
}


const playerItems = document.querySelectorAll('.player');

const closeVideo = (player, video) => {
    player.classList.remove('player--playing');
    video.controls = false;
    video.pause();
};

playerItems.forEach((item) => {

    item.addEventListener('click', (e) => {
        const player = (item).classList.contains('player') ? item : item.querySelector('.player'),
            video = player.querySelector('.player__video');

        if (player.classList.contains('player--playing')) {
            if (!e.target.closest('.player__video')) closeVideo(player, video);
        } else {
            const openPlayer = document.querySelector('.player--playing');
            if (openPlayer) {
                const openPlayerVideo = openPlayer.querySelector('.player__video');
                closeVideo(openPlayer, openPlayerVideo);
                openPlayer.classList.remove('player--playing');
            }
            player.classList.add('player--playing');
            video.controls = true;
            video.autoplay = true;
            video.play();
        }
    });

});


const collapseItems = document.querySelectorAll('.collapse__item');

collapseItems.forEach((item) => {
    item.querySelector('.collapse__title').addEventListener('click', (e) => {
        const openCollapse = document.querySelector('.collapse--open');
        if (openCollapse && !e.target.parentElement.classList.contains('collapse--open')) openCollapse.classList.remove('collapse--open');
        item.classList.toggle('collapse--open');
        scroll('.collapse--open');
    });
});

const stories = document.querySelectorAll('.stories__item');

stories.forEach((story) => {
    story.querySelector('.stories__expand').addEventListener('click', () => {
        stories.forEach((story) => {
            story.classList.add('stories--expanded');
        });
    });
});

const chooses = document.querySelectorAll('.choose');

chooses.forEach(choose => {
    let select = choose.querySelector('.choose__select');
    let defaultValue = choose.querySelector('.choose__value');
    defaultValue.innerText = select.selectedOptions[0].innerText;
    select.addEventListener('change', (event) => {
        let value = event.target.parentNode.querySelector('.choose__value');
        value.innerText = event.target.selectedOptions[0].innerText;
    });
});

mouseEnterChanger('exp-exchange__item');
mouseEnterChanger('universal__item');

function mouseEnterChanger(selector) {
    const items = document.querySelectorAll(`.${selector}`);

    items.forEach(item => {
        item.addEventListener('mouseenter', () => {
            const className = `${selector}--selected`;
            document.querySelector(`.${className}`).classList.remove(className);
            item.classList.add(className);
        });
    });
}

const appointmentButtons = document.querySelectorAll('.button--appointment'),
    appointmentForm = document.querySelector('.appointment-request');

appointmentButtons.forEach(button => {
    button.addEventListener('click', () => {
        appointmentForm.classList.add('appointment-request--show');
    })
});

appointmentForm.addEventListener('click', (e) => {
    if (!e.target.closest('.appointment-request__container')) {
        appointmentForm.classList.remove('appointment-request--show');
        const openResponse = document.querySelector('.response--visible');
        if (openResponse) openResponse.classList.remove('response--visible');
    }
});

document.addEventListener('keyup', e => {
    if (e.key === 'Escape') {
        appointmentForm.classList.remove('appointment-request--show');
        const activePlayer = document.querySelector('.player--playing');
        if (activePlayer) closeVideo(activePlayer, activePlayer.querySelector('.player__video'));
        const openResponse = document.querySelector('.response--visible');
        if (openResponse) openResponse.classList.remove('response--visible');
    }
});

const multipleSwitchCarousel = (value) => {
    carouselsList.multiple.destroy();
    document.getElementById('multiple').removeAttribute('id');
    document.querySelector(`.multiple__list[data-carousel='${value}']`).setAttribute('id', 'multiple');
    carouselsList.multiple = tns(carouselsSettings.find(carousel => carousel.container === '#multiple'));
};

const multipleSelect = document.querySelector('.multiple .choose__select');

multipleSelect && multipleSelect.addEventListener('change', (e) => {
    const optionValue = e.target.selectedOptions[0].value;
    multipleSwitchCarousel(optionValue);
});


const multiplePeriods = document.querySelectorAll('.multiple__period');
multiplePeriods.forEach(period => {
    period.addEventListener('click', (e) => {
        const className = 'multiple__period--active';
        document.querySelector('.multiple__period--active').classList.remove(className);
        const periodValue = period.getAttribute('data-period');
        multipleSwitchCarousel(periodValue);
        period.classList.add(className);
        document.querySelector('.multiple__name').innerHTML = e.target.innerText;
    });
});

const connectionMore = document.querySelector('.connection__more');
connectionMore.addEventListener('click', () => {
    const header = document.querySelector('.header');
    const afterHeader = header.nextElementSibling;
    let className = '';
    afterHeader.classList.value.split(' ').map((value) => {
        if (value) className += `.${value}`;
    });
    scroll(className);
});

const contactAnchors = document.querySelectorAll('.contacts__anchor');
contactAnchors.forEach((contact) => {
    contact.addEventListener('click', () => {
        scroll(`.section--map`);
    });
});

const navAnchor = document.querySelector('.nav--anchor');
navAnchor.addEventListener('click', () => {
    scroll(`.section--contacts`);
});

multipleSelect && multipleSelect.addEventListener('change', (e) => {
    const optionValue = e.target.selectedOptions[0].value;
    multipleSwitchCarousel(optionValue);
});

const carouselsSettings = [
    {
        container: '#multiple',
        controlsPosition: 'bottom',
        navPosition: 'bottom',
        loop: false,
        controlsText: ['', ''],
        items: 1.35,
        gutter: 16,
        preventScrollOnTouch: 'auto',
        preventActionWhenRunning: true,
        swipeAngle: 30,
        responsive: {
            1280: {
                items: 3,
                gutter: 24
            }
        }
    },
    {
        container: '#exp-exchange',
        controlsPosition: 'bottom',
        navPosition: 'bottom',
        loop: false,
        controlsText: ['', ''],
        items: 1.35,
        gutter: 16,
        preventScrollOnTouch: 'auto',
        preventActionWhenRunning: true,
        swipeAngle: 30,
        responsive: {
            1280: {
                items: 3,
                gutter: 24
            }
        }
    },
    {
        container: '#standards',
        controlsPosition: 'bottom',
        navPosition: 'top',
        loop: false,
        controlsText: ['', ''],
        items: 1.35,
        preventScrollOnTouch: 'auto',
        preventActionWhenRunning: true,
        swipeAngle: 30,
        responsive: {
            1280: {
                disable: true
            }
        }
    },
    {
        container: '#comfort',
        controlsPosition: 'bottom',
        navPosition: 'bottom',
        loop: false,
        controlsText: ['', ''],
        items: 1.35,
        gutter: 16,
        preventScrollOnTouch: 'auto',
        preventActionWhenRunning: true,
        swipeAngle: 30,
        responsive: {
            1280: {
                items: 3.5,
                gutter: 24
            }
        }
    },
    {
        container: '#doctors',
        controlsPosition: 'bottom',
        navPosition: 'bottom',
        loop: false,
        controlsText: ['', ''],
        items: 1.35,
        gutter: 48,
        preventScrollOnTouch: 'auto',
        preventActionWhenRunning: true,
        responsive: {
            1280: {
                items: 2.25,
                gutter: 116
            }
        }
    },
    {
        container: '#doctors2',
        controlsPosition: 'bottom',
        navPosition: 'bottom',
        loop: false,
        controlsText: ['', ''],
        items: 1.35,
        gutter: 48,
        preventScrollOnTouch: 'auto',
        preventActionWhenRunning: true,
        responsive: {
            1280: {
                items: 2.25,
                gutter: 116
            }
        }
    },
    {
        container: '#reviews',
        controlsPosition: 'bottom',
        navPosition: 'bottom',
        loop: false,
        controlsText: ['', ''],
        swipeAngle: 30,
        preventScrollOnTouch: 'auto',
        preventActionWhenRunning: true,
        responsive: {
            1280: {
                items: 2,
                gutter: 80,
                controlsPosition: 'top'
            }
        }
    },
    {
        container: '#stories',
        controlsPosition: 'bottom',
        navPosition: 'bottom',
        loop: false,
        controlsText: ['', ''],
        // autoHeight: true
    },
    {
        container: '#stages',
        controlsPosition: 'bottom',
        navPosition: 'bottom',
        loop: false,
        controlsText: ['', ''],
        items: 1.35,
        gutter: 16,
        preventScrollOnTouch: 'auto',
        preventActionWhenRunning: true,
        swipeAngle: 30,
        responsive: {
            1280: {
                items: 2.8,
                gutter: 24
            }
        }
    },
    {
        container: '#pricing',
        controlsPosition: 'bottom',
        navPosition: 'bottom',
        loop: false,
        controlsText: ['', ''],
        items: 1.2,
        preventScrollOnTouch: 'auto',
        preventActionWhenRunning: true,
        swipeAngle: 30,
        responsive: {
            1280: {
                disable: true
            }
        }
    },
    {
        container: '#consultations',
        controlsPosition: 'bottom',
        navPosition: 'bottom',
        loop: false,
        controlsText: ['', ''],
        items: 1.3,
        preventScrollOnTouch: 'auto',
        preventActionWhenRunning: true,
        swipeAngle: 30,
        responsive: {
            1280: {
                disable: true
            }
        }
    },
    {
        container: '#consultations1',
        controlsPosition: 'bottom',
        navPosition: 'bottom',
        loop: false,
        controlsText: ['', ''],
        items: 1.3,
        preventScrollOnTouch: 'auto',
        preventActionWhenRunning: true,
        swipeAngle: 30,
        responsive: {
            1280: {
                disable: true
            }
        }
    },
    {
        container: '#consultations2',
        controlsPosition: 'bottom',
        navPosition: 'bottom',
        loop: false,
        controlsText: ['', ''],
        items: 1.3,
        preventScrollOnTouch: 'auto',
        swipeAngle: 30,
        responsive: {
            1280: {
                disable: true
            }
        }
    },
];

let carouselsList = {};

carouselsSettings.forEach((settings) => {
    let carouselName = settings.container.replace('#', '');
    if (document.querySelector(settings.container)) carouselsList[carouselName] = tns(settings);
});

let scrollPosition = 0;
const support = document.querySelector('.support');

const scrollPreSets = () => {

    document.documentElement.dataset.scroll = (window.scrollY > 5) ? window.scrollY : 0;

    additionalClasses(overhead.clientHeight, 'overhead--fixed');
    additionalClasses(document.querySelector('.header').clientHeight, 'overhead--transition');
};

const storeScroll = () => {

    scrollPreSets();

    if (scrollPosition > document.querySelector('.header').clientHeight) {
        support.classList.add('support--visible');
        if (Math.abs(document.body.getBoundingClientRect().top) < scrollPosition) overhead.classList.add('overhead--visible');
        else overhead.classList.remove('overhead--visible');

        const sectionContacts = document.querySelector('.section--contacts');
        console.log(getCoords(sectionContacts.previousElementSibling).top);

        if(scrollPosition > getCoords(sectionContacts).top - sectionContacts.clientHeight) support.classList.remove('support--visible');

    } else {
        support.classList.remove('support--visible');
        overhead.classList.remove('overhead--visible');
    }

    scrollPosition = Math.abs(document.body.getBoundingClientRect().top);
};

const additionalClasses = (height, className) => {
    if (scrollPosition >= height && !overhead.classList.contains(className)) overhead.classList.add(className);
    else if (scrollPosition <= height || window.scrollY === 0) overhead.classList.remove(className);
};

scrollPosition = Math.abs(document.body.getBoundingClientRect().top);
scrollPreSets();

window.addEventListener('scroll', storeScroll, {passive: true});

function getCoords(elem) {
    let box = elem.getBoundingClientRect();
    return {
        top: box.top + pageYOffset,
        left: box.left + pageXOffset
    }
}

function scroll(selector) {
    let element = document.querySelector(selector);

    if (element) {
        let width = document.body.clientWidth,
            menuHeight = (width >= 1280) ? 0 : 72,
            top = getCoords(element).top - menuHeight,
            nav = document.querySelector('.nav--show');

        if (nav) nav.classList.remove('nav--show');

        window.scrollTo({
            top,
            behavior: 'smooth'
        });
    }
}

const checkVisibilities = () => {
    const sections = document.querySelectorAll('.section');
    sections.forEach((item) => {
        if(!item.classList.contains('section--thisVisible')) {
            if(item.offsetTop < Math.round(window.scrollY + window.innerHeight * .5)) {
                item.classList.add('section--thisVisible');
            }
        }
        else {
            if(item.offsetTop > Math.round(window.scrollY + window.innerHeight)) {
                item.classList.remove('section--thisVisible');
            }
        }
    });
};

window.addEventListener('scroll', checkVisibilities, {passive: true});

checkVisibilities();



