const {series, parallel, src, dest, watch} = require('gulp');
const babel = require('gulp-babel');
const stylus = require('gulp-stylus');
const axis = require('axis');
const concat = require('gulp-concat');
const rename = require('gulp-rename');
const tinypng = require('gulp-tinypng-compress');
const twig = require('gulp-twig');
const uglify = require('gulp-uglify-es').default;
const iconfont = require('gulp-iconfont');
const autoprefixer = require('gulp-autoprefixer');
const iconfontCss = require('gulp-iconfont-css');


const keys = ['2ZH6Bj39JhmzgB4pbMxFzC5VpBDVQ17Y', '75yD2Rx641rTCHBKsqY5kzqG8w1x0GHR', 'df0SxLbddqbK188sFYcg1VZwN2LRTrlR', 'fSrD2YR6dwjyZ0mZRjrR4TtfX6LZNNBc', '5sVZX6Kh8cR8rttzNCw7lxl4PvCQGH6x', 'pGztycrf6nTvZg8MCbysf1VyX7YmKpsq', '58XNW6KTd1Rm0fwVk52WjFtYN91Ctm20', 'f7kZVZzFmSkrytlSppbWtTl1bKFxl5bn', 'sjpZSm0xD18qGgYBN6TBdZJ6DxJ5jMyH', 'VnDp9Kz3zhQ3PsQvkdFVY5JDSvGB2NBZ']
const rand = keys[Math.floor(Math.random() * keys.length)];

const paths = {
    stylus: {
        src: ['./stylus/main.styl'],
        dest: './css/',
        rename: 'style.min.css'
    },
    images: {
        src: './images/**/*',
        dest: './images'
    },
    icons: {
        src: './images/icons/*.svg',
        dest: './fonts'
    },
    templates: {
        src: './templates/*.twig',
        dest: '.'
    },
    js: {
        src: './js/src/*.js',
        dest: './js'
    }
};

function templates() {
    return src(paths.templates.src)
        .pipe(twig())
        .pipe(dest(paths.templates.dest));
}

function icons() {
    return src(paths.icons.src)
        .pipe(iconfontCss({
            fontName: 'icons',
            path: './stylus/_icons.styl',
            targetPath: '../stylus/icons.styl',
            fontPath: '../fonts/'
        }))
        .pipe(iconfont({
            fontName: 'icons',
            prependUnicode: true,
            normalize: true,
            fontHeight: 500
        }))
        .pipe(dest(paths.icons.dest));
}

function tinify() {
    return src(paths.images.src)
        .pipe(tinypng({
            key: rand,
            sigFile: 'images/.tinypng-sigs',
            log: true
        }))
        .pipe(dest(paths.images.dest));
}

function stylesheets() {
    return src(paths.stylus.src)
        .pipe(stylus({
            use: [axis()],
            compress: true,
        }))
        .pipe(autoprefixer())
        .pipe(rename(paths.stylus.rename))
        .pipe(dest(paths.stylus.dest))

}

function js() {
    return src(paths.js.src)
        .pipe(babel())
        .pipe(concat('main.min.js'))
        .pipe(uglify())
        .pipe(dest(paths.js.dest));
}

function watchFiles() {
    watch(['./stylus/*.styl', './stylus/**/*.styl'], stylesheets);
    watch(['./templates/**/*.twig', './templates/**/**/*.twig'], templates);
    watch(['./js/src/*.js'], js);
}

exports.stylesheets = stylesheets;
exports.templates = templates;
exports.icons = icons;
exports.tinify = tinify;
exports.watch = watchFiles;
exports.js = js;
exports.default = series(
    parallel(
        stylesheets,
        js
    ),
    watchFiles
);
